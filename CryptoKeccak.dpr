﻿program CryptoKeccak;

{$APPTYPE CONSOLE}

uses
  Windows,
  unitKeccak in 'unitKeccak.pas';

const
  HexTable: array[0..15] of AnsiChar = '0123456789ABCDEF';

procedure PrintData( title: string; val: PByte; size: integer );
var
  i, j: UInt32;
  data: PUInt8Array absolute val;
begin
  write( title );

  if size > 0 then
    for i := 0 to size-1 do
    begin
      Write( HexTable[ (Ord(data[i]) AND $F0) SHR 4 ], HexTable[ Ord(data[i]) AND $F ], ' ' );

      if (i <> 0) AND ((1+i) mod 16 = 0) then
      begin
        writeln;
        for j := 1 to Length(title) do
          write( ' ' );
      end;
    end;
  writeln;
end;

function LengthInBits( val: AnsiString ): UInt64; inline;
begin
	Result := (sizeof(val[1]) * 8) * Length(val);
end;

procedure HashString_224A( val: AnsiString );
var
  ctx: TSpongeState;
  HashVal: array[0..27] of byte;
begin
  ZeroMemory( @ctx, sizeof(ctx) );
  Assert( Keccak_Init( @ctx, 224 ) = SUCCESS );
  if length(val) > 0 then
  begin
    Assert( Keccak_Update( @ctx, @val[1], LengthInBits(val) ) = SUCCESS );
  end;
  Assert( Keccak_Final( @ctx, @HashVal ) = SUCCESS );

  if Length(val) > 0 then
    PrintData( 'Input:  ', @val[1], length(val) )
  else
    PrintData( 'Input:  ', nil, 0 );
  PrintData( 'Hash:   ', @HashVal, SizeOf(HashVal) );
  writeln; writeln; writeln;
end;

procedure HashString_256A( val: AnsiString );
var
  ctx: TSpongeState;
  HashVal: array[0..31] of byte;
begin
  ZeroMemory( @ctx, sizeof(ctx) );
  Assert( Keccak_Init( @ctx, 256 ) = SUCCESS );
  if length(val) > 0 then
  begin
    Assert( Keccak_Update( @ctx, @val[1], LengthInBits(val) ) = SUCCESS );
  end;
  Assert( Keccak_Final( @ctx, @HashVal ) = SUCCESS );

  if Length(val) > 0 then
    PrintData( 'Input:  ', @val[1], length(val) )
  else
    PrintData( 'Input:  ', nil, 0 );
  PrintData( 'Hash:   ', @HashVal, SizeOf(HashVal) );
  writeln; writeln; writeln;
end;

procedure HashString_384A( val: AnsiString );
var
  ctx: TSpongeState;
  HashVal: array[0..47] of byte;
begin
  ZeroMemory( @ctx, sizeof(ctx) );
  Assert( Keccak_Init( @ctx, 384 ) = SUCCESS );
  if length(val) > 0 then
  begin
    Assert( Keccak_Update( @ctx, @val[1], LengthInBits(val) ) = SUCCESS );
  end;
  Assert( Keccak_Final( @ctx, @HashVal ) = SUCCESS );

  if Length(val) > 0 then
    PrintData( 'Input:  ', @val[1], length(val) )
  else
    PrintData( 'Input:  ', nil, 0 );
  PrintData( 'Hash:   ', @HashVal, SizeOf(HashVal) );
  writeln; writeln; writeln;
end;

procedure HashString_512A( val: AnsiString );
var
  ctx: TSpongeState;
  HashVal: array[0..63] of byte;
begin
  ZeroMemory( @ctx, sizeof(ctx) );
  Assert( Keccak_Init( @ctx, 512 ) = SUCCESS );
  if length(val) > 0 then
  begin
    Assert( Keccak_Update( @ctx, @val[1], LengthInBits(val) ) = SUCCESS );
  end;
  Assert( Keccak_Final( @ctx, @HashVal ) = SUCCESS );

  if Length(val) > 0 then
    PrintData( 'Input:  ', @val[1], length(val) )
  else
    PrintData( 'Input:  ', nil, 0 );
  PrintData( 'Hash:   ', @HashVal, SizeOf(HashVal) );
  writeln; writeln; writeln;
end;


var
{  vals: array[0..4] of AnsiString = (
  	   #$C6#$F5#$0B#$B7#$4E#$29,
  	   #$C1#$EC#$FD#$FC,
  	   #$41#$FB,
       #$CC,
       ''
    );}
  vals: array[0..0] of AnsiString = (
		#$1F#$87#$7C
    );


  fncs: array[0..0] of procedure( val: AnsiString ) = ( HashString_256A{, HashString_256B, HashString_512} );
//  fncs: array[0..3] of procedure( val: AnsiString ) = ( HashString_224A, HashString_256A, HashString_384A, HashString_512A );

var
  i, j: UInt32;

begin
  for i := low(fncs) to high(fncs) do
  begin
    for j := low(vals) to high(vals) do
    begin
      fncs[i]( vals[j] );
    end;

    readln;

    writeln;
    writeln('----------');
    writeln; writeln;
  end;

   readln;
end.
