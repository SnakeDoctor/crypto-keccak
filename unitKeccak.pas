unit unitKeccak;
interface
uses
  windows;

{ $DEFINE KECCAK_EXPOSE_INTERNAL_FUNCTIONS}
{ $DEFINE INLINE_HELPERS}

{$REGION 'Generic Types'}
type
  PUInt8  = ^UInt8;
  PUInt32 = ^UInt32;
  PUInt64 = ^UInt64;

  TUInt8Array  = array[0..(MAXDWORD div 2 )-1] of UInt8;
  TUInt32Array = array[0..(MAXDWORD div 8 )-1] of UInt32;
  TUInt64Array = array[0..(MAXDWORD div 16)-1] of UInt64;

  PUInt8Array  = ^TUInt8Array;
  PUInt32Array = ^TUInt32Array;
  PUInt64Array = ^TUInt64Array;
{$ENDREGION}

{$REGION 'Keccak Types'}
const
	KeccakPermutationSize = 1600;
	KeccakPermutationSizeInBytes = (KeccakPermutationSize div 8);
	KeccakMaximumRate = 1536;
	KeccakMaximumRateInBytes = (KeccakMaximumRate div 8);
	nrRounds = 24;
    nrLanes = 25;

type
	EHashReturn = (SUCCESS = 0, FAIL = 1, BAD_HASHLEN = 2);

	TSpongeState = record
        state: array[0..KeccakPermutationSizeInBytes-1] of UInt8;
        dataQueue: array[0..KeccakMaximumRateInBytes-1] of UInt8;
        rate: UInt32;
        capacity: UInt32;
        bitsInQueue: UInt32;
        fixedOutputLength: UInt32;
        squeezing: UInt8;
        bitsAvailableForSqueezing: UInt32;
	end;
    PSpongeState = ^TSpongeState;
{$ENDREGION}

{$REGION 'Keccak Internal Functions'}
{$IFDEF KECCAK_EXPOSE_INTERNAL_FUNCTIONS}
function InitSponge(ctx: PSpongeState; rate: UInt32; capacity: UInt32): integer;
procedure AbsorbQueue(ctx: PSpongeState);
function Absorb(ctx: PSpongeState; const data: PUInt8Array; databitlen: UInt32): integer;
procedure PadAndSwitchToSqueezingPhase(ctx: PSpongeState);
function Squeeze(ctx: PSpongeState; output: PUInt8Array; outputLength: UInt32): integer;

procedure KeccakPermutation( state: PUInt8Array );
procedure KeccakPermutationAfterXor( state: PUInt8Array; const data: PUInt8Array; dataLengthInBytes: UInt32 );
procedure KeccakPermutationOnWords( state: PUInt64Array );
procedure KeccakInitializeRoundConstants;
procedure KeccakInitializeRhoOffsets;
procedure KeccakInitialize;
procedure KeccakInitializeState(state: PUInt8Array);
procedure KeccakInitializeDataQueue(state: PUInt8Array);
procedure KeccakAbsorb(state: PUInt8Array; const data: PUInt8Array; laneCount: UInt32);
procedure KeccakExtract(state: PUInt8Array; data: PUInt8Array; laneCount: UInt32);
{$ENDIF}
{$ENDREGION}

function Keccak_Init(state: PSpongeState; hashbitlen: integer): EHashReturn;
function Keccak_Update(state: PSpongeState; const data: Pointer; databitlen: UInt64): EHashReturn;
function Keccak_Final(state: PSpongeState; hashval: Pointer): EHashReturn;

implementation

{$REGION 'Keccak Internal Variables'}
var
	KeccakRoundConstants: array[0..nrRounds-1] of UInt64;
	KeccakRhoOffsets: array[0..nrLanes-1] of UInt32;
{$ENDREGION}

{$REGION 'Keccak Internal Function Forwardsd'}
{$IFNDEF KECCAK_EXPOSE_INTERNAL_FUNCTIONS}
function InitSponge(ctx: PSpongeState; rate: UInt32; capacity: UInt32): integer; forward;
procedure AbsorbQueue(ctx: PSpongeState); forward;
function Absorb(ctx: PSpongeState; const data: PUInt8Array; databitlen: UInt32): integer; forward;
procedure PadAndSwitchToSqueezingPhase(ctx: PSpongeState); forward;
function Squeeze(ctx: PSpongeState; output: PUInt8Array; outputLength: UInt32): integer; forward;

procedure KeccakPermutation( state: PUInt8Array ); forward;
procedure KeccakPermutationAfterXor( state: PUInt8Array; const data: PUInt8Array; dataLengthInBytes: UInt32 ); forward;
procedure KeccakPermutationOnWords( state: PUInt64Array ); forward;
procedure KeccakInitializeRoundConstants; forward;
procedure KeccakInitializeRhoOffsets; forward;
procedure KeccakInitialize; forward;
procedure KeccakInitializeState(state: PUInt8Array); forward;
procedure KeccakInitializeDataQueue(state: PUInt8Array); forward;
procedure KeccakAbsorb(state: PUInt8Array; const data: PUInt8Array; laneCount: UInt32); forward;
procedure KeccakExtract(state: PUInt8Array; data: PUInt8Array; laneCount: UInt32); forward;
{$ENDIF}
{$ENDREGION}

{$REGION 'Keccak Helper Functions'}
function index(x, y: UInt32): UInt32; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
    Result := (x mod 5) + 5*(y mod 5);
end;

function _ROL32(a: UInt32; offset: Byte): UInt32; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
	Result := a;
	if offset <> 0 then
    begin
		Result := (a SHL offset) XOR (a SHR (32-offset));
    end;
end;

function _SHL64_a(a: UInt64; offset: Byte): UInt64; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
    Result := UInt64(a) SHL offset;
end;

function _SHL64_b(a: UInt64; offset: Byte): UInt64; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
var
	inA, outA: record
        case Integer of
            0: (Val: UInt64);
            1: (Arr: array[0..1] of UInt32);
    end;
begin
	inA.Val := a;

    if offset = 0 then
    begin
    	outA.Val := inA.Val;
    end else if offset = 32 then
    begin
    	outA.Arr[0] := 0;
    	outA.Arr[1] := inA.Arr[0];
    end else if offset >= 64 then
    begin
    	outA.Val := 0;
    end else if (offset > 0) AND (offset < 32) then
    begin
        outA.Arr[0] := inA.Arr[0] SHL offset;
        outA.Arr[1] := (inA.Arr[1] SHL offset) XOR (inA.Arr[0] SHR (32-offset));
    end else if (offset > 32) AND (offset < 64) then
    begin
		outA.Arr[0] := 0;
        outA.Arr[1] := inA.Arr[0] SHL (offset - 32)
    end;

    Result := outA.Val;
end;

function _SHL64(a: UInt64; offset: Byte): UInt64; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
    Result := _SHL64_b( a, offset );
end;

function _SHR64(a: UInt64; offset: Byte): UInt64; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
    Result := UInt64(a) SHR offset;
end;

function _XOR64(a,b: UInt64): UInt64; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
	Result := UInt64(a) XOR UInt64(b);
end;

function _ROL64(a: UInt64; offset: Byte): UInt64; {$IFDEF INLINE_HELPERS}inline;{$ENDIF}
begin
	Result := a;
	if offset <> 0 then
    begin
		Result := _XOR64( _SHL64(a, offset), _SHR64(a, 64-offset) );
    end;
end;
{$ENDREGION}

{$REGION 'Sponge Functions'}
function InitSponge;
begin
    if (rate+capacity) <> 1600 then
    begin
        Result := 1;
        exit;
    end;

    if (rate <= 0) OR (rate >= 1600) OR ((rate mod 64) <> 0) then
    begin
        Result := 1;
        exit;
    end;

    KeccakInitialize();
	ctx.rate := rate;
    ctx.capacity := capacity;
    ctx.fixedOutputLength := 0;
    KeccakInitializeState( @ctx.state[0] );
    KeccakInitializeDataQueue( @ctx.dataQueue[0] );
    ctx.bitsInQueue := 0;
    ctx.squeezing := 0;
    ctx.bitsAvailableForSqueezing := 0;

    Result := 0;
end;

procedure AbsorbQueue;
begin
    // state->bitsInQueue is assumed to be equal to state->rate
    KeccakAbsorb( PUInt8Array(@ctx.state[0]), PUInt8Array(@ctx.dataQueue[0]), ctx.rate div 64);
    ctx.bitsInQueue := 0;
end;

function Absorb;
var
	i, j, wholeBlocks: UInt64;
    partialBlock, partialByte: UInt32;
    curData: PUint8Array;
    mask: UInt8;
begin
    Result := 0;
    if (ctx.bitsInQueue mod 8) <> 0 then
    begin
        Result := 1; // Only the last call may contain a partial byte
        exit;
    end;
    if ctx.squeezing <> 0 then
    begin
        Result := 1; // Too late for additional input
        exit;
    end;

    i := 0;
    while(i < databitlen) do
    begin
        if ((ctx.bitsInQueue = 0) AND (databitlen >= ctx.rate) AND (i <= (databitlen - ctx.rate))) then
        begin
            wholeBlocks := (databitlen-i) div ctx.rate;
            curData := @data[i div 8];

            //for j := 0 to wholeBlocks-1 do
            j := 0;
            while j < wholeBlocks do
            begin
            	KeccakAbsorb(@ctx.state[0], curData, ctx.rate div 64);

                j := j + 1;
                curData := @curData[ctx.rate div 8];
            end;

            i := i + (wholeBlocks * ctx.rate);
        end else
        begin
            partialBlock := databitlen - i;
            if (partialBlock + ctx.bitsInQueue) > ctx.rate then
                partialBlock := ctx.rate - ctx.bitsInQueue;

            partialByte := partialBlock mod 8;
            partialBlock := partialBlock - partialByte;

            //memcpy(ctx.dataQueue + (ctx.bitsInQueue div 8), data+ (i div 8), partialBlock div 8);
            CopyMemory( @ctx.dataQueue[ctx.bitsInQueue div 8], @data[i div 8], partialBlock div 8 );

            ctx.bitsInQueue := ctx.bitsInQueue + partialBlock;
            i := i + partialBlock;
            if ctx.bitsInQueue = ctx.rate then
                AbsorbQueue(ctx);
            if partialByte > 0 then
            begin
                mask := (1 SHL partialByte)-1;
                ctx.dataQueue[ctx.bitsInQueue div 8] := data[i div 8] AND mask;
                ctx.bitsInQueue := ctx.bitsInQueue + partialByte;
                i := i + partialByte;
            end;
        end;
    end;
end;

procedure PadAndSwitchToSqueezingPhase;
begin
    // Note: the bits are numbered from 0=LSB to 7=MSB
    if ctx.bitsInQueue + 1 = ctx.rate then
    begin
        ctx.dataQueue[ctx.bitsInQueue div 8] := ctx.dataQueue[ctx.bitsInQueue div 8] OR (1 SHL (ctx.bitsInQueue mod 8));
        AbsorbQueue( ctx );
        //memset(state->dataQueue, 0, state->rate/8);
        FillMemory( @ctx.dataQueue[0], ctx.rate div 8, 0 );
    end else
    begin
        //memset(state->dataQueue + (state->bitsInQueue+7)/8, 0, state->rate/8 - (state->bitsInQueue+7)/8);
        FillMemory( @ctx.dataQueue[(ctx.bitsInQueue+7) div 8], ctx.rate div 8 - (ctx.bitsInQueue+7) div 8, 0 );

        ctx.dataQueue[ctx.bitsInQueue div 8] := ctx.dataQueue[ctx.bitsInQueue div 8] OR (1 SHL (ctx.bitsInQueue mod 8));
    end;

    ctx.dataQueue[(ctx.rate-1) div 8] := ctx.dataQueue[(ctx.rate-1) div 8] OR (1 SHL ((ctx.rate-1) mod 8));
    AbsorbQueue( ctx );

    KeccakExtract( @ctx.state[0], @ctx.dataQueue[0], ctx.rate div 64);
    ctx.bitsAvailableForSqueezing := ctx.rate;

    ctx.squeezing := 1;
end;

function Squeeze;
var
	i: UInt64;
	partialBlock: UInt32;
begin
    if ctx.squeezing = 0 then
        PadAndSwitchToSqueezingPhase( ctx );

    if (outputLength mod 8) <> 0 then
    begin
        Result := 1; // Only multiple of 8 bits are allowed, truncation can be done at user level
        exit;
    end;

    i := 0;
    while i < outputLength do
    begin
        if ctx.bitsAvailableForSqueezing = 0 then
        begin
            KeccakPermutation(@ctx.state[0]);
            KeccakExtract(@ctx.state[0], @ctx.dataQueue[0], ctx.rate div 64);
            ctx.bitsAvailableForSqueezing := ctx.rate;
        end;
        partialBlock := ctx.bitsAvailableForSqueezing;
        if partialBlock > outputLength - i then
            partialBlock := outputLength - i;

        //memcpy(output+i/8, state->dataQueue+(state->rate-state->bitsAvailableForSqueezing)/8, partialBlock/8);
        CopyMemory( @output[i div 8], @ctx.dataQueue[(ctx.rate-ctx.bitsAvailableForSqueezing) div 8], partialBlock div 8);
        ctx.bitsAvailableForSqueezing := ctx.bitsAvailableForSqueezing - partialBlock;
        i := i + partialBlock;
    end;
    Result := 0;
end;
{$ENDREGION}

{$REGION 'KeccakF-1600 Functions'}
procedure KeccakPermutation;
begin
{#if (PLATFORM_BYTE_ORDER != IS_LITTLE_ENDIAN)
    UINT64 stateAsWords[KeccakPermutationSize/64];
#endif

#if (PLATFORM_BYTE_ORDER == IS_LITTLE_ENDIAN)}

    KeccakPermutationOnWords( PUInt64Array(state) );

{#else
    fromBytesToWords(stateAsWords, state);
    KeccakPermutationOnWords(stateAsWords);
    fromWordsToBytes(state, stateAsWords);
#endif }
end;

procedure KeccakPermutationAfterXor;
var
	i: UInt32;
begin
    for i := 0 to dataLengthInBytes-1 do
    begin
        state[i] := state[i] xor data[i];
    end;
    KeccakPermutation(state);
end;

procedure theta(A: PUInt64Array);
var
	x,y: UInt32;
    C, D: array[0..4] of UInt64;
begin
    for x := 0 to 4 do
    begin
        C[x] := 0;
        for y := 0 to 4 do
            C[x] := _XOR64( C[x], A[index(x, y)] );
    end;

    for x := 0 to 4 do
        D[x] := _XOR64( _ROL64(C[(x+1) mod 5], 1), C[(x+4) mod 5] );

    for x := 0 to 4 do
        for y := 0 to 4 do
            A[index(x, y)] := _XOR64( A[index(x, y)], D[x] );
end;

procedure rho(A: PUInt64Array);
var
	x,y: UInt32;
begin
    for x := 0 to 4 do
    	for y := 0 to 4 do
        	A[index(x, y)] := _ROL64(A[index(x, y)], KeccakRhoOffsets[index(x, y)]);
end;

procedure pi(A: PUInt64Array);
var
	x,y: UInt32;
    tempA: array[0..24] of UInt64;
begin
    for x := 0 to 4 do
    	for y :=0 to 4 do
        	tempA[index(x, y)] := A[index(x, y)];

    for x := 0 to 4 do
    	for y :=0 to 4 do
        	A[index(0*x+1*y, 2*x+3*y)] := tempA[index(x, y)];
end;

procedure chi(A: PUInt64Array);
var
	x,y: UInt32;
    C: array[0..4] of UInt64;
begin
    for y := 0 to 4 do
    begin
        for x := 0 to 4 do
        begin
            //C[x] := XOR64( A[index(x, y)], (((NOT A[index(x+1, y)])+1) AND A[index(x+2, y)]) );
            C[x] := _XOR64( A[index(x, y)], ((NOT A[index(x+1, y)]) AND A[index(x+2, y)]) );
        end;

        for x := 0 to 4 do
            A[index(x, y)] := C[x];
    end;
end;

procedure iota(A: PUInt64Array; indexRound: UInt32);
begin
    A[index(0, 0)] := _XOR64( A[index(0, 0)], KeccakRoundConstants[indexRound] );
end;


procedure KeccakPermutationOnWords;
var
	i: UInt32;
begin
    for i := 0 to nrRounds-1 do
    begin
        theta(state);
        rho(state);
        pi(state);
        chi(state);
        iota(state, i);
    end;
end;

function LFSR86540(LFSR: PUInt8): boolean;
begin
    Result := (LFSR^ AND 1) <> 0;
    if ((LFSR^ AND $80) <> 0) then
    begin
        // Primitive polynomial over GF(2): x^8+x^6+x^5+x^4+1
        LFSR^ := UInt8((UInt32(LFSR^) SHL 1) xor $71);
    end else
    begin
        LFSR^ := UInt32(LFSR^) SHL 1;
    end;
end;

procedure KeccakInitializeRoundConstants;
var
	LFSRstate: UInt8;
    i, j, bitPosition: UInt32;
begin
    LFSRstate := $01;

    for i := 0 to nrRounds-1 do
    begin
        KeccakRoundConstants[i] := 0;
        for j := 0 to 6 do
		begin
            bitPosition := (1 SHL j)-1; //2^j-1
            if LFSR86540(@LFSRstate) then
            begin
				//KeccakRoundConstants[i] := KeccakRoundConstants[i] xor (UInt64(1) SHL bitPosition);
              	KeccakRoundConstants[i] := _XOR64( KeccakRoundConstants[i], _SHL64(UInt64(1), bitPosition));
            end;
        end;
	end;
end;

procedure KeccakInitializeRhoOffsets;
var
	x, y, t, newX, newY: UInt32;
begin
	KeccakRhoOffsets[index(0, 0)] := 0;
    x := 1;
	y := 0;
	for t := 0 to 24-1 do
    begin
        KeccakRhoOffsets[index(x, y)] := ((t+1)*(t+2) div 2) mod 64;
        newX := (0*x+1*y) mod 5;
		newY := (2*x+3*y) mod 5;
        x := newX;
		y := newY;
    end;
end;

procedure KeccakInitialize;
begin
	KeccakInitializeRoundConstants;
	KeccakInitializeRhoOffsets;
end;

procedure KeccakInitializeState;
begin
    FillMemory(state, KeccakPermutationSizeInBytes, 0);
end;

procedure KeccakInitializeDataQueue;
begin
    FillMemory(state, KeccakMaximumRateInBytes, 0);
end;

procedure KeccakAbsorb;
begin
    KeccakPermutationAfterXor(state, data, laneCount*8);
end;

procedure KeccakExtract;
begin
    CopyMemory( @data[0], @state[0], laneCount*8 );
end;
{$ENDREGION}

{$REGION 'NIST Interface Wrappers'}
function Keccak_Init;
begin
	Result := SUCCESS;
    case hashbitlen of
        0: begin // Default parameters, arbitrary length output
            InitSponge(state, 1024, 576);
        end;

        224: begin
            InitSponge(state, 1152, 448);
        end;

        256: begin
            InitSponge(state, 1088, 512);
        end;

        384: begin
            InitSponge(state, 832, 768);
        end;

        512: begin
			InitSponge(state, 576, 1024);
        end

        else
            Result := BAD_HASHLEN;
    end;
    state.fixedOutputLength := hashbitlen;
end;

function Keccak_Update;
var
	lastByte: UInt8;
begin
    if (databitlen mod 8) = 0 then
    begin
        Result := EHashReturn( Absorb(state, data, databitlen) );
    end else
    begin
        Result := EHashReturn( Absorb(state, data, databitlen - (databitlen mod 8)) );
        if Result = SUCCESS then
        begin
            // Align the last partial byte to the least significant bits
            lastByte := PUInt8Array(data)[databitlen div 8] SHR (8 - (databitlen mod 8));
            Result := EHashReturn( Absorb(state, @lastByte, databitlen mod 8) );
        end;
    end;
end;

function Keccak_Final;
begin
    Result := EHashReturn( Squeeze(state, hashval, state.fixedOutputLength) );
end;
{$ENDREGION}

initialization
  //..
finalization
  //..
end.
